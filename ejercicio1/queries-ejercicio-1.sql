-- 1
select cod_art 
from articulo
where precio between 100 and 1000 
and descripcion like 'A%'

--2
select *
from proveedor

--3
select descripcion
from material
where cod_mat in (1,3,6,9,18)

--4
select cod_prov, nombre
from proveedor
where domicilio like '%Suipacha%'
 and fecha_alta >= '20010101'
 and fecha_alta < '20020101'

--5 
select p.nombre, c.nombre
from proveedor p
left join ciudad c on p.cod_ciu = c.cod_ciu

--6 
select p.nombre
from proveedor p
left join ciudad c on p.cod_ciu = c.cod_ciu
where c.nombre = 'La Plata'

--7
select c.nro 
from contiene c 
inner join articulo a on c.cod_art = a.cod_art
where a.descripcion like 'A%'

--8
select m.cod_mat, m.descripcion
from material m
inner join provisto_por pp on m.cod_mat = pp.cod_mat
inner join proveedor p on pp.cod_prov = p.cod_prov
inner join ciudad c on p.cod_ciu = c.cod_ciu
where c.ciudad = 'La Plata'

--9
select p.nombre
from proveedor p
inner join provisto_por pp on p.cod_prov = pp.cod_prov
inner join material m on pp.cod_mat = m.cod_mat
inner join compuesto_por cp on m.cod_mat = cp.cod_mat
inner join contiene c on cp.cod_art = c.cod_art
inner join almacen a on c.nro = a.nro
where a.responsable = 'Martin Gomez'

-- 10
update contiene c 
	set c.nro = 20
where c.nro = 10

-- 11
delete from proveedor p
	where p.name like '%ABC%'

--12
select count(*)
from proveedor
where nomre like 'F%'

--13
select a.nombre, AVG(art.precio)
from almacen a
inner join contiene c on a.nro = c.nro
inner join articulo art on c.cod_art = art.cod_art
group by 1

--14
select a.descripcion
from articulo a
inner join compuesto_por cp on a.cod_art = cp.cod_art
group by 1
having count(a.descripcion) > 2

--15
select p.cod_prov, p.nombre, p.domicilio, count(*) as cant_materiales
from proveedor p
inner join provisto_por pp on p.cod_prov = pp.cod_prov
group by 1,2,3
