ALTER ROLE "user" WITH SUPERUSER;

GRANT ALL PRIVILEGES ON DATABASE "unlam-db" TO "user";

--Almacen (nro, nombre, responsable)
--Articulo (cod_art, descripción, precio)
--Material (cod_mat, descripción)
--Proveedor (cod_prov, nombre, domicilio, cod_ciu, fecha_alta)
--Ciudad (cod_ciu, nombre)
--Contiene (nro, cod_art)
--Compuesto_por (cod_art, cod_mat)
--Provisto_por (cod_mat, cod_prov)

CREATE TABLE almacen (
	 nro int AUTO_INCREMENT PRIMARY KEY,
	 nombre varchar(140) NOT NULL,
	 responsable varchar (140) NOT NULL
 );

 INSERT INTO almacen (nro, nombre, responsable) VALUES 
 	(1, 'El Pepe', 'Juan Jose'),
	(2, 'El Almacen', 'Sergio'),
	(3, 'Full', 'Juaquin'),
	(4, 'De todito', 'Alejandro'),
	(5, 'Pedro almacen', 'Pedro');
 
CREATE TABLE articulo (
	cod_art int AUTO_INCREMENT PRIMARY KEY,
	descripcion varchar(300) NOT NULL,
	precio decimal(10,2) NOT NULL
);

INSERT INTO articulo (cod_art, descripcion, precio) VALUES 
	(1, 'Iphone', 1200),
	(2, 'Xiami', 1200),
	(3, 'Samsung', 1200),
	(4, 'Lg', 1200),
	(5, 'Motorola', 1200);

CREATE TABLE material (
	cod_mat int AUTO_INCREMENT PRIMARY KEY,
	descripcion varchar(300) NOT NULL
);

INSERT INTO material (cod_mat, descripcion) VALUES 
	(1, 'Aluminio'),
	(2, 'Metal'),
	(3, 'Estaneo'),
	(4, 'Bronce'),
	(5, 'Cobre');

CREATE TABLE ciudad (
	cod_ciudad int AUTO_INCREMENT PRIMARY KEY,
	nombre varchar (100) NOT NULL
);

INSERT INTO ciudad (cod_ciudad, nombre) VALUES 
	(1, 'Mar del plata'),
	(2, 'La plata'),
	(3, 'CABA'),
	(4, 'Miramar'),
	(5, 'Las totninas');
 
CREATE TABLE contiene (
	nro int,
	cod_art int,
	FOREIGN KEY (nro) REFERENCES almacen (nro),
	FOREIGN KEY (cod_art) REFERENCES articulo (cod_art)
);

INSERT INTO contiene (nro, cod_art) VALUES 
	(1, 1),
	(1, 2),
	(1, 3),
	(2, 5),
	(2, 4),
	(3, 3),
	(3, 3),
	(4, 1),
	(5, 2);

CREATE TABLE proveedor (
	cod_prov int AUTO_INCREMENT PRIMARY KEY,
	nombre varchar(140) NOT NULL,
	domicilio varchar(140) NOT NULL,
	cod_ciudad int,
	fecha_alta date NOT NULL,
	FOREIGN KEY (cod_ciudad) REFERENCES ciudad (cod_ciudad)
);

INSERT INTO proveedor (cod_prov, nombre, domicilio, cod_ciudad, fecha_alta) values
	(1, 'juan', 'Rivadavia 123', 1, '2020-10-22'), 
	(2, 'jose', 'Cabildo 123', 1, '2020-10-22'),
	(3, 'pedro', 'Suipacha 3123', 1, '2020-10-22'),
	(4, 'alejandro', 'Calle falsa 123', 1, '2020-10-22'),
	(5, 'sebastian', 'Libertador 123', 1, '2020-10-22');
 
CREATE TABLE compuesto_por (
	cod_art int,
	cod_mat int,
	FOREIGN KEY (cod_mat) REFERENCES material (cod_mat),
	FOREIGN KEY (cod_art) REFERENCES articulo (cod_art)
);

INSERT INTO compuesto_por (cod_art, cod_mat) VALUES 
	(1, 1),
	(1, 2),
	(1, 3),
	(2, 5),
	(2, 4),
	(3, 3),
	(3, 3),
	(4, 1),
	(5, 2);


CREATE TABLE provisto_por (
	cod_mat int,
	cod_prov int,
	FOREIGN KEY (cod_mat) REFERENCES material (cod_mat),
	FOREIGN KEY (cod_prov) REFERENCES proveedor (cod_prov)
);

INSERT INTO provisto_por (cod_mat, cod_prov) VALUES 
	(1, 1),
	(1, 2),
	(1, 3),
	(2, 5),
	(2, 4),
	(3, 3),
	(3, 3),
	(4, 1),
	(5, 2);




