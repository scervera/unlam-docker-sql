# unlam-docker-sql

1. descargar docker desktop

https://www.docker.com/products/docker-desktop

2. clone this project

3. run `docker-compose -f ./compose-sql.yml up`

4. conectar con tu cliente favorito (dbeaver, datagrip, etc)

5. ready

# Comandos de docker

> para listar los contenedores `docker ps -a`

> parar un contenedor `docker stop <id_contenedor>`

> eliminar un contenedor `docker rm <id_contenedor>`